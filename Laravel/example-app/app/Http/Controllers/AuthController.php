<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register (){
        return view("/register");
    }

    public function save (Request $request) {
        $namaDepan = $request->input('Firstname');
        $namaBelakang = $request->input('Lastname');

    return view("/dashboard", ["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);

}
}