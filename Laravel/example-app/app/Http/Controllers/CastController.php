<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view("cast.addCast");
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required|max:3',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request-> input('nama'),
            'umur' => $request-> input('umur'),
            'bio' => $request-> input('bio')
        ]);

        return redirect('/cast');
    }

    public function index(){
        $cast= DB::table('cast')->get();

        return view ('cast.viewCast', ['cast' => $cast]);
    }

    public function show($id){
        $casts = DB::table('cast')->find($id);

        return view ('cast.detailCast', ['casts' => $casts]);
    }

    public function edit($id){

        $casts = DB::table('cast')->find($id);

        return view ('cast.editCast', ['casts' => $casts]);
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required|max:3',
            'bio' => 'required',
    ]);

    DB::table('cast')
              ->where('id', $id)
              ->update([
                'nama' => $request->input('nama'),
                'umur' => $request->input('umur'),
                'bio' => $request->input('bio'),
              ]);

              return redirect ('/cast');
}
    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect ('/cast');
    }
}