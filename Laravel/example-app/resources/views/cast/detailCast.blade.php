@extends('layout.master')

@section('title')
    Halaman Bio Cast
@endsection

@section('content')
<h1>{{$casts->nama}}</h1>
<p>{{$casts->bio}}</p>
@endsection