@extends('layout.master')

@section('title')
    Halaman Add Cast
@endsection

@section('content')
<form action="/cast/{{$casts->id}}" method="POST">
  @csrf
  @method('PUT')
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="form-group">
      <label>Name</label>
      <input type="text" name='nama' value="{{$casts->nama}}" class="form-control">
    </div>
    <div class="form-group">
      <label>Umur</label>
      <input type="text" name='umur' value="{{$casts->umur}}" class="form-control">
    </div>
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$casts->bio}}</textarea>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection