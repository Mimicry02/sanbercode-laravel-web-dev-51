@extends('layout.master')

@section('title')
    Halaman View Cast
@endsection

@section('content')

<a href="/cast/addCast" class="btn btn-primary my-3">Add Cast</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>

@forelse ($cast as $key => $caster)

<tr>
    <th scope="row">{{$key+1}}</th>
    <td>{{$caster->nama}}</td>
    <td>{{$caster->umur}}</td>
    <td>
        <a href="/cast{{$caster->id}}" class="btn btn-sm btn-info">Detail</a>
        <a href="/cast/{{$caster->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
        <form action="/cast/{{$caster->id}}" method="POST">
        @csrf
        @method('delete')
        <input type="submit" value="Delete" class="btn btn-sm btn-danger">
        </form>
      </td>
</tr>

@empty
    <p>No users</p>
@endforelse

    </tbody>
  </table>
@endsection