<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel Sanbercode - Register</title>
</head>
<body>
    <h1>Buat account baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/dashboard" method="POST">
        @csrf
        <label for="Firstname">First Name</label><br><br>
        <input type="text" name="Firstname"><br><br>
        <label for="Lastname">Last Name</label><br><br>
        <input type="text" name="Lastname"><br><br>
        <label for="Gender">Gender :</label><br>
        <input type="radio" value="1" name="Gender"> Male <br>
        <input type="radio" value="2" name="Gender"> Female <br>
        <input type="radio" value="3" name="Gender"> Other <br><br>
        <label for="">Nationality :</label><br>
        <select name="" id="">
            <option value="1">Indonesian</option>
            <option value="2">Singaporean</option>
            <option value="3">Malaysian</option>
            <option value="4">Australian</option>
        </select><br><br>
        <label for="Language">Language Spoken :</label><br><br>
        <input type="checkbox"> Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>
        <label for="Bio">Bio :</label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Signup"></button>
    </form>
</body>
</html>