<?php
require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

$sheep = new Animal("shaun");

echo "Name : " .$sheep->name ."<br>";
echo "Legs : " .$sheep->legs. "<br>";
echo "Cold Blooded : " .$sheep->cold_blooded."<br> <br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$frog = new frog("buduk");

echo "Name : " .$frog->name ."<br>";
echo "Legs : " .$frog->legs. "<br>";
echo "Cold Blooded : " .$frog->cold_blooded."<br>";
echo "Jump : " .$frog->jump() ."<br> <br>";

$ape = new ape("kera sakti");

echo "Name : " .$ape->name ."<br>";
echo "Legs : " .$ape->legs. "<br>";
echo "Cold Blooded : " .$ape->cold_blooded."<br>";
echo "Yell : " .$ape->yell() ."<br>";

?>